## Introdução

Este projeto contém o exemplo de chamadas REST da API *Assinatura Eletrônica* via Postman.
Dessa forma é necessário ter o [Postman](https://www.postman.com/downloads/) instalado.

O exemplo irá acessar o serviço de *Assinatura Eletrônica* no ambiente HOM.
Sua equipe deve solicitar o acesso a este ambiente.

O [pdf Postman](postman_README_v5.pdf) contém instruções de uso do Postman
para melhor entendimento do exemplo.

Basicamente, para efetuar uma assinatura eletrônica é necessário coletar as evidências e enviá-las para a API.
Posteriormente, é possível requisitar o relatório das evidências.

O [BRy4Devs](https://api-assinatura.hom.bry.com.br/api-assinatura-eletronica) (aba de Assinatura Eletrônica)
contém mais detalhes sobre os parâmetros das rotas.
